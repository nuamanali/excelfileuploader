﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace EcxelFileUploader
{

    public partial class WebForm1 : System.Web.UI.Page
    {
        static String ExcelPath;
        static string constr = "server=localhost;port=3306;database=employee;username=root;password=root";
        private MySqlConnection con = new MySqlConnection(constr);
           
        protected void Page_Load(object sender, EventArgs e)
        {
            Button3.Visible = false;
            TextBox1.Visible = false;
            Label4.Visible = false;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string path = Path.GetFileName(FileUpload1.FileName);
            path = path.Replace(" ", "");
            FileUpload1.SaveAs(Server.MapPath("~/ExcelFile/") + path);
            ExcelPath = Server.MapPath("~/ExcelFile/") + path;
            OleDbConnection mycon = new OleDbConnection("Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " +
                                                        ExcelPath +
                                                        "; Extended Properties=Excel 8.0; Persist Security Info = False");
            mycon.Open();
            OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", mycon);
            OleDbDataAdapter da = new OleDbDataAdapter();
            da.SelectCommand = cmd;
            DataSet ds = new DataSet();
            da.Fill(ds);
            GridView2.DataSource = ds.Tables[0];
            GridView2.DataBind();
            mycon.Close();
            Label2.Text = "Excel File Has Been Upload and Data Captured";
            GridView2.Visible = true;
            Button2.Visible = true;
            Button3.Visible = true;
            TextBox1.Visible = true;
            Label4.Visible = true;
        }

        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void Button2_Click1(object sender, EventArgs e)
        {
            int idemp;
            String fname;
            String lname;
            int contact;
            string qualification;

            OleDbConnection mycon = new OleDbConnection("Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " +
                                                        ExcelPath +
                                                        "; Extended Properties=Excel 8.0; Persist Security Info = False");
            mycon.Open();
            OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", mycon);
            OleDbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                idemp = Convert.ToInt32(dr[0].ToString());
                fname = dr[1].ToString();
                lname = dr[2].ToString();
                contact = Convert.ToInt32(dr[3].ToString());
                qualification = dr[4].ToString();
                savedata(idemp, fname, lname, contact, qualification);
            }

            Label3.Text = "Data Has Been Saved Succcessfully in Database";
            mycon.Close();
        }

        private void savedata(int id, String firstname1, String lastname1, int contact1, string qualification1)
        {
           
            MySqlConnection conn = new MySqlConnection(constr);
            using (conn)
            {
                try
                {
                    conn.Open();
                    String query = "INSERT INTO employee.emp_excel(idemp,fname,lname,contact,qualification) VALUES(@id,@fn,@ln,@cont,@quali)";

                    MySqlCommand cmd = new MySqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("id", id);
                    cmd.Parameters.AddWithValue("fn", firstname1);
                    cmd.Parameters.AddWithValue("ln", lastname1);
                    cmd.Parameters.AddWithValue("cont", contact1);
                    cmd.Parameters.AddWithValue("quali", qualification1);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                catch (MySqlException)
                {
                    throw;
                }
            }
        }
        

        protected void Button3_Click(object sender, EventArgs e)
        {
            MySqlConnection connect = new MySqlConnection(constr);
            connect.Open();
            string query = "select * from employee.emp_excel where idemp=" + TextBox1.Text;
            MySqlCommand com = new MySqlCommand(query, con);
            MySqlDataAdapter da = new MySqlDataAdapter(query, con);
            da.SelectCommand = com;
            DataSet ds = new DataSet();
            da.Fill(ds);
            GridView2.DataSource = ds;
            GridView2.DataBind();
            TextBox1.Text = "";
            Label2.Text = "success";
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void GridView2_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView2.PageIndex = e.NewPageIndex;
            MySqlConnection conm = new MySqlConnection(constr);
            conm.Open();
            MySqlCommand cmd = new MySqlCommand("select * from employee.emp_excel", con);
            DataSet ds = new DataSet();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(ds);
            GridView2.DataSource = ds;
            GridView2.DataBind();
            conm.Close();
            Button3.Visible = true;
            Label4.Visible = true;
            TextBox1.Visible = true;
            Button2.Visible = true;
        }
    }
}