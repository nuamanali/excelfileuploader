﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="EcxelFileUploader.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>  
            <table>  
                <tr>  
                    <td>   
  
        Select File  
  
        </td>  
                    <td>  
                        <asp:FileUpload ID="FileUpload1" runat="server" ToolTip="Select Only Excel File" />  
                    </td>  
                    <td>  
                        <asp:Button ID="Button1" runat="server" Text="Upload" onclick="Button1_Click" />  
                    </td>  
                    <td>  
                        &nbsp;</td>  
                </tr>  
            </table>  
            <table>  
                <tr>  
                    <td>  
                        <p>  
                            <asp:Label ID="Label2" runat="server" Text="label"></asp:Label>  
                        </p>  
                    </td>  
                </tr>  
            </table>  
            <asp:Label ID="Label4" runat="server" Text="Search"></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
            <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Go" />
            <asp:GridView ID="GridView2" runat="server" Height="160px" Width="575px" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="GridView2_SelectedIndexChanged" OnPageIndexChanging="GridView2_OnPageIndexChanging" PageSize="4">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="idemp" HeaderText="empid" />
                    <asp:BoundField DataField="fname" HeaderText="first name" />
                    <asp:BoundField DataField="lname" HeaderText="last name" />
                    <asp:BoundField DataField="contact" HeaderText="contact" />
                    <asp:BoundField DataField="qualification" HeaderText="Qualification" />
                </Columns>
                <EditRowStyle BackColor="#7C6F57" />
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerSettings FirstPageText="Next" LastPageText="Previous" Mode="NumericFirstLast" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
        </div>
        <asp:Button ID="Button2" runat="server" Height="22px" OnClick="Button2_Click1" Text="save to database" Visible="False" Width="111px" />
        <p>
            <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
        </p>
    </form>
</body>
</html>
